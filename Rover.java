class Rover {

    public static void main(String[] args) {
        int [][] map = {
            { 9, 15,  8, 16,  8, 10,  8, 8},
            {10, 11, 15, 15, 10, 11, 14, 8},
            {14, 13, 13, 9,  11, 12, 12, 14},
            {13, 11, 14, 9,  13, 14, 14, 9},
            {13, 13, 16, 9,  16, 12, 14, 13},
            {12, 12, 11, 13, 11, 15, 14, 11},
            {12, 14, 15, 14,  9, 15, 15, 14},
            {16, 18, 12, 13,  8, 14, 14, 16},
            {13, 11, 15, 14, 14, 11, 11, 10},

    };
        calculateRoverPath(map);
    }

    public static void calculateRoverPath(int[][] map) {
        int res = -1;
        int fuel = 1;
        if (map != null && map.length > 0 && map[0].length > 0) {
            int height = map.length;
            int width = map[0].length;

            int[][] cost = new int[height][width];

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (i == 0 && j == 0) {
                        cost[i][j] = map[i][j] + fuel;
                    } else if (i == 0) {
                        cost[i][j] = map[i][j] + cost[i][j - 1];
                    } else if (j == 0) {
                        cost[i][j] = map[i][j] + cost[i - 1][j];
                    } else {
                        int top = cost[i - 1][j];
                        int left = cost[i][j - 1];
                        int min = Math.min(top, left);
                        cost[i][j] = map[i][j] + min  ;
                    }
                }
            }
            try(FileWriter writer = new FileWriter("notes.txt", false)) {
                // запись всей строки
                res = cost[height - 1][width - 1];
                writer.write("result:" + res);
            }
            catch(IOException ex){
                System.out.println(ex.getMessage());
            }
        }
    }
}